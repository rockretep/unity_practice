﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleScript : MonoBehaviour {

	public float speed;


	// Use this for initialization
	void Start () {
		ForPractice ("testingFor");
		WhilePractice ("testingWhile");
	}
	
	// Update is called once per frame
	void Update () {
		MovePaddle ();
		
	}

	void MovePaddle() {

		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.position += Vector3.down * speed * Time.deltaTime;
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			transform.position += Vector3.up * speed * Time.deltaTime;
		}


	}

	void ForPractice(string str) {
		for (int i = 0; i < 10; i++) {
			print (str + i);
		}
	}

	void WhilePractice(string str) {
		int i = 0;
		while (i < 10) {
			print (str + i);
			i++;
		}
	}
}
